import React from 'react'

import { Box, Link as MuiLink } from '@mui/material'
import { useStyles } from './HomePage.useStyles'
import Link from 'next/link'

export function HomePage() {
  const classes = useStyles()

  return (
    <Box className={classes.root}>
      <Link href='/olympic/create'>
        <MuiLink variant='h6' href='#'>
          Создать соревнование
        </MuiLink>
      </Link>
    </Box>
  )
}
