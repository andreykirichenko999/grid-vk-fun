import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles(() => ({
  container: {
    padding: '32px',
    backgroundColor: '#fff'
  },
  layout: {
    padding: '32px',
    backgroundColor: '#eee',
    boxSizing: 'border-box',
    display: 'flex',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 2,
    minHeight: '100vh'
  }
}))
