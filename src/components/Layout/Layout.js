import React from 'react'
import Head from 'next/head'
import { Box, Container } from '@mui/material'

import { useStyles } from './Layout.useStyles'

export const Layout = ({ children }) => {
  const classes = useStyles()

  return (
    <Box className={classes.layout}>
      <Head>
        <html lang='ru' />
        <title>VK Grid</title>
        <meta name='description' content='Vk game grid 2021' />
        <link rel='icon' href='/favicon.ico' />
        <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap' />
      </Head>
      <Container maxWidth='sm' className={classes.container}>
        {children}
      </Container>
    </Box>
  )
}
