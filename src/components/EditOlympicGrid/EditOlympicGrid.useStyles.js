import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles(() => ({
  title1: {
    marginBottom: '16px',
    backgroundColor: '#fff'
  },
  title2: {
    marginBottom: '16px',
    marginTop: '16px',
    backgroundColor: '#fff'
  },
  error: {
    marginBottom: '32px',
    color: 'red'
  },
  list: {
    margin: 0,
    marginBottom: '16px',
    padding: 0
  },
  item: {
    listStyle: 'decimal inside'
  }
}))
