import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Box, Typography, Link as MuiLink } from '@mui/material'
import Link from 'next/link'
import axios from 'axios'
import { useStyles } from './EditOlympicGrid.useStyles'

import { Pair } from '../Pair/Pair'

export const EditOlympicGrid = ({ baseUrl }) => {
  const [isError, setIsError] = useState(false)
  const [winner, setWinner] = useState(null)
  const [winners, setWinners] = useState([])
  const [stages, setStages] = useState([])
  const router = useRouter()
  const { id } = router.query

  useEffect(() => {
    if (!id) {
      return
    }

    axios
      .get(`/api/olympic/${id}`)
      .then((result) => {
        if (result.data.winner) {
          setWinner(result.data.winner)
          return
        }

        setStages(result.data.stages)
      })
      .catch(() => {
        setIsError(true)
      })
  }, [id])

  const save = (team) => {
    setWinners([...winners, team])
  }

  useEffect(() => {
    if (!stages[stages.length - 1]) {
      return
    }

    if (stages[stages.length - 1].length && winners.length === stages[stages.length - 1].length) {
      axios
        .post(`/api/olympic/${id}`, { id, winners })
        .then((result) => {
          setWinners([])
          setStages(result.data.stages)
          if (result.data.winner) {
            setWinner(result.data.winner)
          }
        })
        .catch(() => {
          setIsError(true)
        })
    }
  }, [winners, stages])

  if (isError) {
    return <Box>Такого соревнования не существует</Box>
  }

  const link = `${baseUrl}/olympic/${id}`
  const classes = useStyles()

  return (
    <Box>
      <Typography component='h1' variant='h6' className={classes.title1}>
        Секретная ссылка
      </Typography>

      <Link href={link}>
        <MuiLink href={link}>{link}</MuiLink>
      </Link>

      <Typography component='h1' variant='h6' className={classes.title2}>
        Этап {stages.length}. Выбираем команды победители
      </Typography>

      {!winner && !!stages.length && !!stages[stages.length - 1].length && (
        <Box component='ul' className={classes.list}>
          {stages[stages.length - 1].map((pair) => {
            return <Pair key={pair.pairId} save={save} pair={pair} />
          })}
        </Box>
      )}

      {!!winner && <Box>Победитель - {winner.name}</Box>}
    </Box>
  )
}
