import React, { useState } from 'react'

import { Box, Button, TextField, Stack } from '@mui/material'
import { useStyles } from './CreateTeam.useStyles'

export function CreateTeam({ onSave, onCancel }) {
  const classes = useStyles()

  const [name, setName] = useState('')

  const handleChange = (event) => {
    setName(event.target.value)
  }

  const onSaveClick = () => {
    onSave(name)
  }

  const onCancelClick = () => {
    onCancel()
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.field}>
        <TextField
          fullWidth
          id='name'
          label='Имя команды/участника'
          onChange={handleChange}
          value={name}
          variant='standard'
        />
      </Box>

      <Stack direction='row-reverse' spacing={2}>
        <Button onClick={onSaveClick} required variant='outlined' color='success'>
          Сохранить
        </Button>

        <Button onClick={onCancelClick}>Отмена</Button>
      </Stack>
    </Box>
  )
}
