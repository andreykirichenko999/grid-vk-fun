import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles(() => ({
  root: {
    marginBottom: '32px',
    marginTop: '32px'
  },
  title: {
    marginBottom: '16px',
    backgroundColor: '#fff'
  },
  field: {
    marginBottom: '16px'
  }
}))
