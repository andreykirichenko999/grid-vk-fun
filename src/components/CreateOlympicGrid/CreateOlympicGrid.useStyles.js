import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles(() => ({
  title: {
    marginBottom: '32px',
    backgroundColor: '#fff'
  },
  error: {
    marginBottom: '32px',
    color: 'red'
  },
  list: {
    margin: 0,
    marginBottom: '16px',
    padding: 0
  },
  item: {
    listStyle: 'decimal inside'
  }
}))
