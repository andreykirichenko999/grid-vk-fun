import React, { useState } from 'react'
import { Box, Typography, Button } from '@mui/material'
import { useRouter } from 'next/router'

import axios from 'axios'

import { CreateTeam } from '../CreateTeam/CreateTeam'
import { useStyles } from './CreateOlympicGrid.useStyles'

const TEAM_MAX = 8

export const CreateOlympicGrid = () => {
  const [teams, setTeams] = useState([])
  const [isOpened, setIsOpened] = useState(false)
  const [isSameNameError, setIsSameNameError] = useState(false)
  const classes = useStyles()

  const onAddClick = () => {
    setIsOpened(true)
    setIsSameNameError(false)
  }

  const onAdd = (name) => {
    setIsOpened(false)
    if (teams.some((item) => item === name)) {
      setIsSameNameError(true)
    } else {
      setTeams([...teams, name])
    }
  }

  const onAddCancel = () => {
    setIsOpened(false)
  }

  const [isCreatingError, setIsCreatingError] = useState(false)

  const router = useRouter()

  const save = async () => {
    try {
      const result = await axios.post('/api/olympic/create', { teams })

      router.push(`/olympic/${result.data.id}`)
    } catch (error) {
      setIsCreatingError(true)
    }
  }

  if (isCreatingError) {
    return <Box>Ошибка создания турнира</Box>
  }

  return (
    <Box>
      <Typography component='h1' variant='h4' className={classes.title}>
        Новая турнирная таблица
      </Typography>

      <Box component='ul' className={classes.list}>
        {teams.map((team) => {
          return (
            <Box component='li' className={classes.item} key={team}>
              {team}
            </Box>
          )
        })}
      </Box>

      {isSameNameError && <Box className={classes.error}>Такаое имя команды уже есть</Box>}

      {teams.length < TEAM_MAX && (
        <Button variant='outlined' onClick={onAddClick}>
          Добавить участника
        </Button>
      )}

      {isOpened && <CreateTeam onSave={onAdd} onCancel={onAddCancel} />}

      {teams.length === TEAM_MAX && (
        <Button variant='outlined' onClick={save}>
          Сохранить
        </Button>
      )}
    </Box>
  )
}
