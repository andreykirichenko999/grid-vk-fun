import React, { useState } from 'react'
import { Box, Button } from '@mui/material'
import { useStyles } from './Pair.useStyles'

export function Pair({ pair, save }) {
  const classes = useStyles()
  const [disabled, setDisabled] = useState(false)

  const onTeam1 = () => {
    save(pair.team1)
    setDisabled(true)
  }

  const onTeam2 = () => {
    save(pair.team2)
    setDisabled(true)
  }

  return (
    <Box className={classes.root}>
      <Button disabled={disabled} onClick={onTeam1} required variant='outlined' color='success'>
        {pair.team1.name}
      </Button>
      &nbsp;VS&nbsp;
      <Button disabled={disabled} onClick={onTeam2} required variant='outlined' color='success'>
        {pair.team2.name}
      </Button>
    </Box>
  )
}
