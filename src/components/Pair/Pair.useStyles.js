import { makeStyles } from '@mui/styles'

export const useStyles = makeStyles(() => ({
  root: {
    marginBottom: '32px',
    marginTop: '32px'
  }
}))
