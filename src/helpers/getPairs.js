import { v4 as uuidv4 } from 'uuid'

export const getPairs = (teams) => {
  const pairs = []
  const values = Object.values(teams)

  for (let i = 0; i < values.length; i += 2) {
    const pairId = uuidv4()

    pairs.push({
      pairId,
      team1: values[i],
      team2: values[i + 1]
    })
  }

  return pairs
}
