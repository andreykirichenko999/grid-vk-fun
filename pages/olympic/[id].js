import React from 'react'

import { EditOlympicGrid } from '../../src/components/EditOlympicGrid/EditOlympicGrid'

function Olympic({ baseUrl }) {
  return <EditOlympicGrid baseUrl={baseUrl} />
}

Olympic.getInitialProps = async (context) => {
  const { req } = context

  let result = {}

  if (req) {
    result.baseUrl = `http://${req.headers.host}`
  } else if (window) {
    result.baseUrl = window.location.origin
  }

  return result
}

export default Olympic
