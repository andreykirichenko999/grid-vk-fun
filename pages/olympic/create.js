import React from 'react'

import { CreateOlympicGrid } from '../../src/components/CreateOlympicGrid/CreateOlympicGrid'

export default function Create() {
  return <CreateOlympicGrid />
}
