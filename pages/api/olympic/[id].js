import fs from 'fs'
import { getPairs } from '../../../src/helpers/getPairs'

export default function handler(req, res) {
  if (req.method === 'GET') {
    const { id } = req.query

    fs.readFile(`./data/olympic/${id}.json`, (error, buffer) => {
      if (error) {
        res.status(400).json({ error })
        return
      }

      const data = JSON.parse(buffer.toString())

      data.id = id
      res.status(200).json({ ...data })
    })
  }

  if (req.method === 'POST') {
    const { id, winners } = req.body

    fs.readFile(`./data/olympic/${id}.json`, (error, buffer) => {
      if (error) {
        res.status(400).json({ error })
      }
      const data = JSON.parse(buffer.toString())

      if (winners.length === 1) {
        data.winner = winners[0]
      } else {
        const pairs = getPairs(winners)
        data.stages.push(pairs)
      }

      const str = JSON.stringify(data)

      fs.writeFile(`./data/olympic/${id}.json`, str, () => {
        res.status(200).json({ id, stages: data.stages, winner: data.winner })
      })
    })
  }
}
