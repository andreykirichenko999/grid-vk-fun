import fs from 'fs'
import { v4 as uuidv4 } from 'uuid'
import { getPairs } from '../../../src/helpers/getPairs'

export default function handler(req, res) {
  if (req.method === 'POST') {
    const id = uuidv4()
    const publicId = uuidv4()

    let result = {
      id,
      publicId,
      stages: [],
      teams: []
    }

    if (Array.isArray(req.body.teams)) {
      req.body.teams.forEach((name) => {
        const teamId = uuidv4()
        result.teams.push({ name, teamId })
      })
    }

    const pairs = getPairs(result.teams)
    result.stages.push(pairs)

    const str = JSON.stringify(result)

    fs.writeFile(`./data/olympic/${id}.json`, str, () => {
      res.status(200).json({ id })
    })
  }
}
