import React from 'react'
import { CssBaseline } from '@mui/material'
import { Layout } from '../src/components/Layout/Layout'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <CssBaseline />
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}

export default MyApp
